# Notes #
## Compiling the code ##
Using CMake 3.1 and up one can compile using cmd or bash.

using g++ (add -Wall for all warnings)

`g++ -std=c++11 -o main.o main.cpp polygon.cpp geoalgs.cpp test/testing.cpp`

**on windows you might want to suffix main.o differently**

**Running the executable produces a test report**

## Code Structure ##
Files are created in pairs of .cpp and .h.
The headers have some inline definitions - but mostly declarations.

tests are placed in a subdirectory

entry point is placed in main, polygon contains the CGeometry namespace which has definitions for geometric shapes.
geoalgs contains the Geomalgs namespace which contains geometric algorithms.

## 1 Add a class, CSquare, that represents a square ##
- Was done by modifying the CPolygon base to have virtual function set_value
- Create CSquare class deriving from CRectangle
- Override set_value and throw an argument error when width and height does not match.

## 2 To each class add a method that calculates the circumference ##
- Add virtual method circumeference to CPolygon
- Override in derived classes
	
## 3 Add a compare operator, >, that compares the area of two objects ##
- Add virtual area function to CPolygon
- overwrite **operator>** in CPolygon and use the area function 

## 4 Generalize the CPolygon class to represent an arbitrary polygon with N line segments ##
- The absraction for a line segment is done using pairs of coordinates
- infix operators for comparison of CPoint's have been defined

- using std::vector to represent the sequence
- sequence only lives during the invocation of the constructor(method scope) makes implicit destructor sufficient

## 5 Add a check for crossing line segments and a suitable way to abort the construction ##
- During the construction of a polygon an validation algorithm is run.
- The validation algorithm is written such that it opts out early if the condition fails.
- Based on the return value an invalid_arg exception is thrown
	
# Testing Notes #
- Did a simple framework for defining cases and running them as part of one test.
- Cases can be reused as the addition is 'pass by value'.

- massive code duplication in testing.cpp (bad)

The proper way would be to include something like CppUnit or Boost.Test or any other similar testing framework which is widely adopted.

- Wrote most of the tests before doing the implementations

- used wolfram alfa for calculating reference results of more complex polygons in area/cirumference test cases
 http://www.wolframalpha.com/input/?i=
 
	- FGHIJK
		- polygon+(0,0)+(2,0)+(3,1)+(3,2)+(2,2)+(0,1)
	- FGHJK
		- polygon+(0,0)+(2,0)+(3,1)+(2,2)+(0,1)
	- FGLIJK
		- polygon+(0,0)+(2,0)+(2,1)+(3,2)+(2,2)+(0,1)

	

	
### External references ###
Detection of line intersection:
	http://geomalgorithms.com/a09-_intersect-3.html
	
Calculation of area:
	https://www.mathopenref.com/coordpolygonarea2.html