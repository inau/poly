
/*Implement the following and add appropriate test cases to main():
  (1) Add a class, CSquare, that represents a square
  (2) To each class add a method that calculates the circumference
  (3) Add a compare operator, >, that compares the area of two objects:
      E.g. the expression (rect > trgl) returns true if the area of rect is larger than trgl.
  (4) Generalize the CPolygon class to represent an arbitrary polygon with N line segments.
      Modify the area and circumference methods accordingly. Implement 
	  constructors and destructors.
  (5) Add a check for crossing line segments and a suitable way to abort the construction
      in this situation.
*/

#include <iostream>
#include <cmath>
#include "polygon.h"
#include "geoalgs.h"

using namespace std;


/****************************************************************
 * (2)
 * To each class add a method that calculates circumference
 * *************************************************************/
//CSquare derives from rectangle so we are good
CGeometry::NumericType CGeometry::CRectangle::circumference() {
    return (2 * width) + (2 * height);
}
CGeometry::NumericType CGeometry::CTriangle::circumference() {
        return width + height + ((NumericType)hypot(width,height));
}

/****************************************************************
 * (4)
 * Generalize the CPolygon class to represent an arbitrary polygon with N line segments.
 * Modify the area and circumference methods accordingly.
 * Implement constructors and destructors.
 * *************************************************************/

CGeometry::CPolygon::CPolygon(vector<CGeometry::CPoint> points) {
    if( points.empty() || points.size() == 1)
        throw std::invalid_argument("No Line segments can be deduced from a single point or no points");

    if( Geomalgs::bruteforce_validation(points, _circumference, _area) ) {
        throw std::invalid_argument("Line segments cross");
    }

    //_area = Geomalgs::shoelace(points);
}
