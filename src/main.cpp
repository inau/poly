//
// Created by Ivan on 09-May-18.
//

#include "test/testing.h"

/******************************
 Entrypoint
 ******************************/
using namespace Tests;
int main () {

    run_test_suites(50);

    std::cout.flush();
    return 0;
}
