#ifndef CPP_POLY_POLYGON_H
#define CPP_POLY_POLYGON_H


#include <stdexcept>
#include <vector>
#include <cmath>
#include <iostream>

namespace CGeometry {

    //globally change the numeric type used
    //alternatively one could do templates
    typedef int NumericType;

    /********************************
     * (4) Line segments (pairs of coordinates)
     ********************************/

    struct CPoint {
        NumericType x, y;

        CPoint() = default;
        CPoint(NumericType ix, NumericType iy) : x(ix), y(iy) {};

        // Infix operator definitions
        bool operator==(const CPoint &other) const {
            return x == other.x && y == other.y;
        };

        bool operator!=(const CPoint &other) const {
            return !this->operator==(other);
        }

        // if they tie on 1 axis check if they differ on other
        bool operator>(const CPoint &other) const {
            return (x == other.x) ? (y == other.y ? false : y > other.y) : x > other.x;
        }
        bool operator<(const CPoint &other) const {
            return (x == other.x) ? (y == other.y ? false : y < other.y) : x < other.x;
        }

        //composed infix ops
        bool operator<=(const CPoint &other) const {
            return this->operator==(other) || this->operator<(other);
        }
        bool operator>=(const CPoint &other) const {
            return this->operator==(other) || this->operator>(other);
        }

    };

    inline std::ostream& operator<<(std::ostream& os, const CGeometry::CPoint &pt)
    {
        os << "(" << pt.x << "," << pt.y << ")";
        return os;
    }

    // Static utility functions for CPoints
    static CGeometry::CPoint POINT_MIN(const CGeometry::CPoint &a, const CGeometry::CPoint &b) {
        return a < b ? a : b;
    }
    static CGeometry::CPoint POINT_MAX(const CGeometry::CPoint &a, const CGeometry::CPoint &b) {
        return a > b ? a : b;
    }

    /**
     * Check if point c is on the line ab.
     * The function uses the comparison of two areas to determine where the c point is compared to the line ab
     * @param a
     * @param b
     * @param b
     * @return 0 for on line | negative value for right of the line | positive value for left of the line
     */
    static CGeometry::NumericType POINT_ON_LINE(const CGeometry::CPoint &a, const CGeometry::CPoint &b, const CGeometry::CPoint &c) {
        //          dx(ab) * dy(ac) - dx(ac)*dy(ab) (area surface of 2 rectangles)
        //  Area of two rectangles is compared, depending on c's position the value changes
        //  Consider a(0,0), b(1,1), c(2,2) the area would be somewhat like the below
        //  the left area would be (1 * 2) and the right (2 * 1) meaning they would cancel out
        //  or as illustrated below areas ox and xø

        //  [o]
        //  [x][ø]

        //  if c was instead(1,2) the area would only be [x] or positive area
        //  if c was (3,2) the area [xø] would be one unit larger resulting in [xø+] or negative area
        return (b.x - a.x) * (c.y - a.y) - (c.x - a.x) *(b.y - a.y);
    }


    class CPolygon {
    protected:
        NumericType width, height;
        NumericType _area = 0, _circumference = 0;
    public:
        /** (3) **/
        virtual NumericType area (void){ return _area; };
        bool operator> (CPolygon &other){ return area() > other.area(); }
        bool operator< (CPolygon &other){ return area() < other.area(); }
        /** (2) **/
        virtual NumericType circumference () { return _circumference; };
        /** (1) to be able to validate input in subclass **/
        virtual void set_values (NumericType a, NumericType b){ width=a; height=b;}

        CPolygon() = default;
        CPolygon(std::vector<CPoint> lines);
    };

    class COutput {
    public:
        void output (NumericType i);
    };

    class CRectangle: public CPolygon, public COutput {
    public:
        NumericType circumference();
        NumericType area (void) { return (width * height); }
    };

    class CTriangle: public CPolygon, public COutput {
    public:
        NumericType circumference();
        NumericType area (void) { return (width * height / 2); }
    };

    /****************************
    * (1)
    * Add CSquare
    ****************************/
    class CSquare : public CRectangle {
    public:
        void set_values (NumericType a, NumericType b) {
            if( a != b ) throw std::invalid_argument("Dimension lengths are not equal.");
            width=a;
            height=b;
        }

    };


}

#endif //CPP_POLY_POLYGON_H
