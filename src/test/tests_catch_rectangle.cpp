#include "catch2.hpp"
#include "../polygon.h"

TEST_CASE( "Rectangle set value", "[rect]" ) {
    CGeometry::CRectangle sq;
    //first var > 2nd
    REQUIRE_NOTHROW( sq.set_values(10,5) );
    REQUIRE_NOTHROW( sq.set_values(9,2) );
    //first var < 2nd
    REQUIRE_NOTHROW( sq.set_values(1,3) );
    REQUIRE_NOTHROW( sq.set_values(2,10) );
    // equal warrs
    REQUIRE_NOTHROW( sq.set_values(2,2) );
    REQUIRE_NOTHROW( sq.set_values(9,9) );
}

TEST_CASE( "Rectangle area", "[rect]" ) {
    CGeometry::CRectangle sq;
    REQUIRE_NOTHROW( sq.set_values(2,4) );
    REQUIRE( sq.area() == 8 );

    REQUIRE_NOTHROW( sq.set_values(4,5) );
    REQUIRE( sq.area() == 20 );

    REQUIRE_NOTHROW( sq.set_values(10,20) );
    REQUIRE( sq.area() == 200 );

    REQUIRE_NOTHROW( sq.set_values(15,25) );
    REQUIRE( sq.area() == 375 );
}

TEST_CASE( "Rectangle circumference", "[rect]" ) {
    CGeometry::CRectangle sq;
    REQUIRE_NOTHROW( sq.set_values(2,4) );
    REQUIRE( sq.circumference() == 12 );

    REQUIRE_NOTHROW( sq.set_values(4,5) );
    REQUIRE( sq.circumference() == 18 );

    REQUIRE_NOTHROW( sq.set_values(12,10) );
    REQUIRE( sq.circumference() == 44 );

    REQUIRE_NOTHROW( sq.set_values(15,25) );
    REQUIRE( sq.circumference() == 80 );
}

/**
SCENARIO( "Square area", "[square]" ) {
    CGeometry::CSquare sq;
    GIVEN( "Square exist" ) {
        sq.set_values(2,2);
        WHEN("Values are valid 2,2") {
            THEN( "area should be calculated and result in 4") {
                REQUIRE( sq.area() ==  4 );
            }
        }
    }
}
 **/