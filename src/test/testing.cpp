//
// Created by Ivan on 08-May-18.
//

#include <cmath>
#include "testing.h"
#include "../polygon.h"
#include "../geoalgs.h"

using namespace CGeometry;
using namespace Tests;

/**
 * tests for following:
 *      dimension restrictions apply
 *      area calculation works
 *
 * @param trials
 */
void run_square_test(int trials) {
    CSquare csq;
    Test<bool> sq_error;
    sq_error.set_description("Square: Invalid Dimension");
    Case<bool> c;

    Test<NumericType > sq_ok;
    sq_ok.set_description("Square: Valid Dimension");

    Case<NumericType> c2;
    for(int i = 1; i < trials; i++) {
        c.set_expected(true);
        c2.set_expected(i*i);
        try {
            csq.set_values( i , i );
            c2.set_actual( csq.area() );
            csq.set_values( i , trials + 1 );
            c.set_actual(false);
        } catch (std::invalid_argument &err) {
            c.set_actual(true);
        }
        sq_error.add_case( c );
        sq_ok.add_case( c2 );
    }

    sq_error.run();
    sq_ok.run();

    /* ToDo: design tests for edgepost cases (int overflows when squaring, negative numbers etc.) */
}

/**
 * tests for following:
 *      circumeference of square, rectangle and triangle
 *      area calculation works
 *
 * @param trials
 */
void run_circumference_tests(int trials) {
    CSquare sq;
    CTriangle tr;
    CRectangle rc;

    Test<NumericType > sqt, trt, rct;
    sqt.set_description("Circumference: square");
    rct.set_description("Circumference: rectangle");
    trt.set_description("Circumference: triangle");

    Case<NumericType > c;

    for(int i = 1; i < trials; i++) {
        c.set_expected( 4*i );
        sq.set_values( i , i );
        c.set_actual( sq.circumference() );
        sqt.add_case(c);

        c.set_expected( 6*i );
        rc.set_values( 2*i , i );
        c.set_actual( rc.circumference() );
        rct.add_case(c);

        c.set_expected( 2*i + ((NumericType)hypot(i,i)) );
        tr.set_values( i , i );
        c.set_actual( tr.circumference() );
        trt.add_case(c);
    }
    sqt.run();
    rct.run();
    trt.run();

    /* ToDo: design tests for edgepost cases (int overflows when summing, negative numbers etc.) */
}

template<class T>
void case_wrapper(T e, T a, Case<T> &c, Test<T> &t, std::string dsc = "") {
    c.set_expected(e);
    c.set_actual(a);
    c.set_description(dsc);
    t.add_case(c);
}

/**
 * tests for following:
 *      Coordinate infix operators
 */
void run_point_tests() {
    CPoint A(0,0),B(1,0),C(1,1), D(0,0);

    Test<bool> t1, t2, t3;
    t1.set_description("Points: equality");
    t2.set_description("Points: inequality <");
    t3.set_description("Points: inequality >");

    Case<bool> c;
    case_wrapper(false, A == B, c, t1);
    case_wrapper(false, C == B, c, t1);
    case_wrapper(true, A == A, c, t1);
    case_wrapper(false, B == A, c, t1);
    case_wrapper(true, A == D, c, t1);
    t1.run();

    case_wrapper(true, A < B, c, t2);
    case_wrapper(false, B < A, c, t2);
    case_wrapper(true, A < C, c, t2);
    case_wrapper(true, B < C, c, t2);
    case_wrapper(false, A < D, c, t2);
    case_wrapper(false, D < A, c, t2);
    t2.run();

    case_wrapper(false, A > B, c, t3);
    case_wrapper(true, B > A, c, t3);
    case_wrapper(false, A > C, c, t3);
    case_wrapper(false, B > C, c, t3);
    case_wrapper(false, A > D, c, t3);
    case_wrapper(false, D > A, c, t3);
    t3.run();

    /** can extend with more infix (<=, >=) , negative values for coordinates etc. **/

}

//might aswell test the new abstraction (pairs of points)
/**
 * Tests for following:
 *      min and max functions for cpoints (getting line points)
 *      line lengths (2 points)
 *      intersection (stacked lines, parallel lines)
 *
 */
void run_line_2_tests() {
    CPoint A(0,0),B(1,0),C(1,1);
    CPoint O(5,5),P(6,6),Q(7,7), R(8,8); // points on same line
    CPoint S(5,6),T(8,9),U(5,4), V(8,7); // points for parallel lines

    Test<CPoint> t1, t2;
    Test<NumericType > t3, t4;
    Test<bool> t5, t6;

    Case<CPoint> c;
    Case<NumericType > c1;
    Case<bool > c2;

    // T1
    t1.set_description("Lines: leftmost points");
    case_wrapper(A, CGeometry::POINT_MIN(A,B), c, t1);
    case_wrapper(A, CGeometry::POINT_MIN(C,A), c, t1);
    //tie on x
    case_wrapper(B, CGeometry::POINT_MIN(B,C), c, t1);
    case_wrapper(B, CGeometry::POINT_MIN(C,B), c, t1);

    // T2
    t2.set_description("Lines: rightmost points");
    case_wrapper(B, CGeometry::POINT_MAX(A,B), c, t2);
    case_wrapper(C, CGeometry::POINT_MAX(C,A), c, t2);
    //tie on x
    case_wrapper(C, CGeometry::POINT_MAX(B,C), c, t2);
    case_wrapper(C, CGeometry::POINT_MAX(C,B), c, t2);

    // T3
    t3.set_description("Lines: length");
    case_wrapper((NumericType)1, Geomalgs::LINE_LENGTH(A, B), c1, t3);
    case_wrapper((NumericType)1, Geomalgs::LINE_LENGTH(B, C), c1, t3);
    case_wrapper((NumericType)1, Geomalgs::LINE_LENGTH(C, B), c1, t3);

    //diagonal
    case_wrapper((NumericType)hypot(1,1), Geomalgs::LINE_LENGTH(C, A), c1, t3);
    case_wrapper((NumericType)hypot(1,1), Geomalgs::LINE_LENGTH(A, C), c1, t3);

    // T4
    t4.set_description("Lines: same point as both ends (length)");
    /** *  Consider if this should actually trigger an exception
        *  Get min/max should also be tested for ties
     **/
    case_wrapper((NumericType)0, Geomalgs::LINE_LENGTH(A, A), c1, t4);
    case_wrapper((NumericType)0, Geomalgs::LINE_LENGTH(B, B), c1, t4);

    // T5 overlaps: consider a line O-P-Q-R (different permutations)
    t5.set_description("Lines: intersection - stacked (same line)");
    case_wrapper(false, Geomalgs::LINE_INTERSECT(O,R,P,Q), c2, t5, "OR PQ");
    case_wrapper(false, Geomalgs::LINE_INTERSECT(O,R,O,Q), c2, t5, "OR OQ");
    case_wrapper(false, Geomalgs::LINE_INTERSECT(O,R,P,R), c2, t5, "OR PR");
    case_wrapper(false, Geomalgs::LINE_INTERSECT(O,R,Q,R), c2, t5, "OR QR");
    case_wrapper(false, Geomalgs::LINE_INTERSECT(O,R,O,P), c2, t5, "OR OP");

    case_wrapper(false, Geomalgs::LINE_INTERSECT(O,Q,P,Q), c2, t5, "OQ PQ");
    case_wrapper(false, Geomalgs::LINE_INTERSECT(O,Q,P,R), c2, t5, "OQ PR");
    case_wrapper(false, Geomalgs::LINE_INTERSECT(O,Q,Q,R), c2, t5, "OQ QR");
    case_wrapper(false, Geomalgs::LINE_INTERSECT(O,Q,O,P), c2, t5, "OQ OP");

    case_wrapper(false, Geomalgs::LINE_INTERSECT(O,P,P,Q), c2, t5, "OP PQ");
    case_wrapper(false, Geomalgs::LINE_INTERSECT(O,P,P,R), c2, t5, "OP PR");
    case_wrapper(false, Geomalgs::LINE_INTERSECT(O,P,Q,R), c2, t5, "OP QR");

    // T6
    t6.set_description("Lines: no intersection - parallel");
    case_wrapper(false, Geomalgs::LINE_INTERSECT(S,T,O,R), c2, t6);
    case_wrapper(false, Geomalgs::LINE_INTERSECT(U,V,O,R), c2, t6);
    case_wrapper(false, Geomalgs::LINE_INTERSECT(S,T,U,V), c2, t6);

    t1.run();
    t2.run();
    t3.run();
    t4.run();
    t5.run();
}

/**
 * Test for following:
 *      construction of polygon (valid, invalid input)
 *      circumference of polygon
 *      area of polygon
 */
void run_poly_test() {
    CPoint A(0,0),B(1,0),C(1,1), D(0,1);

    std::vector<CPoint> points;
    Test<bool> t1;
    t1.set_description("Polygon: construction");
    Case<bool> c1, c2;
    c1.set_expected(true);
    c1.set_description("exception thrown for no input");
    try {
        CPolygon invalid(points);
        c1.set_actual(false);
    } catch (std::invalid_argument &err) {
        c1.set_actual(true);
    }
    t1.add_case(c1);

    c1.set_description("exception thrown for invalid input(crossing)");
    c1.set_expected(true);
    try {
        CPolygon invalid2({A,B,D,C});
        c1.set_actual(false);
    } catch (std::invalid_argument &err) {
        c1.set_actual(true);
    }
    t1.add_case(c1);

    Test<NumericType> t2, t3;
    c1.set_expected(false);
    c1.set_description("exception thrown for valid input");
    t2.set_description("Polygon: circumference");
    t3.set_description("Polygon: area");
    CPoint F(0,0),G(2,0),H(3,1), I(3,2), J(2,2),K(0,1),L(2,1);
    try {
        Case<NumericType> c2;
        CPolygon valid({A,B,C}); //A -- B = 1, AC = hypot, BA = 1
        c2.set_expected( (NumericType)2 + hypot(1,1) );
        c2.set_actual( valid.circumference() );
        t2.add_case(c2);
        c2.set_expected( (NumericType)(1*1) / 2 );
        c2.set_actual( valid.area() );
        t3.add_case(c2);

        //square
        CPolygon valids({A,B,C,D}); //A -- B = 1, AC = hypot, BA = 1
        c2.set_expected( (NumericType)4 );
        c2.set_actual( valids.circumference() );
        t2.add_case(c2);
        c2.set_expected( (NumericType)1 );
        c2.set_actual( valids.area() );
        t3.add_case(c2);

        //FGHIJK
        CPolygon valid1({F,G,H,I,J,K});
        c2.set_expected( (NumericType)(5 + sqrt(2) + sqrt(5)) );
        c2.set_actual( valid1.circumference() );
        t2.add_case(c2);
        c2.set_expected( (NumericType)9 / 2 );
        c2.set_actual( valid1.area() );
        t3.add_case(c2);

        //FGHIJK
        CPolygon valid2({F,G,H,J,K});
        c2.set_expected( (NumericType)(3 + 2*sqrt(2) + sqrt(5)) );
        c2.set_actual( valid2.circumference() );
        t2.add_case(c2);
        c2.set_expected( (NumericType)4 );
        c2.set_actual( valid2.area() );
        t3.add_case(c2);

        //FGLIJK
        CPolygon valid3({F,G,L,I,J,K});
        c2.set_expected( (NumericType)(5 + sqrt(2) + sqrt(5)) );
        c2.set_actual( valid3.circumference() );
        t2.add_case(c2);
        c2.set_expected( (NumericType)7/2 );
        c2.set_actual( valid3.area() );
        t3.add_case(c2);


        c1.set_actual(false);
    } catch (std::invalid_argument &err) {
        c1.set_actual(true);
    }
    t1.add_case(c1);



    t1.run();
    t2.run();
    t3.run();


}

void Tests::run_test_suites(int trials) {
    run_square_test(trials);
    run_circumference_tests(trials);
    run_point_tests();
    run_line_2_tests();
    run_poly_test();
}

template<class T>
int Tests::report_details(T expect, T actual, std::string description, std::ostream &out, std::string OK, std::string FAIL) {
    out << "[" << (expect == actual ? OK : FAIL) << "] " << description;
    if (expect != actual) {
        out << "\tExp: " << expect << " Act: " << actual <<std::endl;
        return 0;
    }
    out << std::endl;
    return 1;
};

int Tests::report(bool passed, std::string description, std::ostream &out, std::string OK, std::string FAIL) {
    out << "[" << (passed ? OK : FAIL) << "] " << description << std::endl;
    return passed;
};
