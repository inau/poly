#include "catch2.hpp"
#include "../polygon.h"

TEST_CASE( "Square set value", "[square]" ) {
    CGeometry::CSquare sq;
    //first var > 2nd
    REQUIRE_THROWS( sq.set_values(10,5) );
    REQUIRE_THROWS( sq.set_values(9,2) );
    //first var < 2nd
    REQUIRE_THROWS( sq.set_values(1,3) );
    REQUIRE_THROWS( sq.set_values(2,10) );
    // equal warrs
    REQUIRE_NOTHROW( sq.set_values(2,2) );
    REQUIRE_NOTHROW( sq.set_values(9,9) );
}

TEST_CASE( "Square area", "[square]" ) {
    CGeometry::CSquare sq;
    REQUIRE_NOTHROW( sq.set_values(2,2) );
    REQUIRE( sq.area() == 4 );

    REQUIRE_NOTHROW( sq.set_values(5,5) );
    REQUIRE( sq.area() == 25 );

    REQUIRE_NOTHROW( sq.set_values(10,10) );
    REQUIRE( sq.area() == 100 );

    REQUIRE_NOTHROW( sq.set_values(15,15) );
    REQUIRE( sq.area() == 225 );
}

TEST_CASE( "Square circumference", "[square]" ) {
    CGeometry::CSquare sq;
    REQUIRE_NOTHROW( sq.set_values(2,2) );
    REQUIRE( sq.circumference() == 8 );

    REQUIRE_NOTHROW( sq.set_values(5,5) );
    REQUIRE( sq.circumference() == 20 );

    REQUIRE_NOTHROW( sq.set_values(10,10) );
    REQUIRE( sq.circumference() == 40 );

    REQUIRE_NOTHROW( sq.set_values(15,15) );
    REQUIRE( sq.circumference() == 60 );
}

/**
SCENARIO( "Square area", "[square]" ) {
    CGeometry::CSquare sq;
    GIVEN( "Square exist" ) {
        sq.set_values(2,2);
        WHEN("Values are valid 2,2") {
            THEN( "area should be calculated and result in 4") {
                REQUIRE( sq.area() ==  4 );
            }
        }
    }
}
 **/