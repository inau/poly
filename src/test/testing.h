//
// Created by Ivan on 08-May-18.
//

#ifndef CPP_POLY_TESTING_H
#define CPP_POLY_TESTING_H

#include <iostream>
#include <vector>

namespace Tests {
#define test_OK "OK"
#define test_FAIL "Fail"

    /************************************
     * Test output
     * **********************************/
    template<class T>
    int report_details(T expect, T actual, std::string description, std::ostream &out, std::string OK, std::string FAIL);

    int report(bool passed, std::string description, std::ostream &out, std::string OK, std::string FAIL);

    /************************************
     * Test objects
     * **********************************/
    template<class T>
    class Case {
    protected:
        std::string description;
        T expected, actual;
    public:
        void set_expected(T exp) {
            expected = exp;
        }
        void set_actual(T act) {
            actual = act;
        }
        void set_description(std::string descr) {
            description = descr;
        }

        bool passed() {
           // std::cerr << expected << " " << actual << std::endl;
            return expected == actual;
        };

        void output(std::ostream &out) { report_details(expected, actual, description, out, " ", "x"); };
    };

    /**
     * A class that encapsulates a series of test cases.
     *
     * @tparam T
     */
    template<class T>
    class Test {
        protected:
            bool passed = true;
            std::string description;
            std::vector<Case<T>> cases;
            int no_passed = 0;

        public:
            void set_description(std::string desc) {
                description = desc;
            };

            void add_case(Case<T> c) {
                cases.push_back(c);
            }

            int total_cases() {
                return cases.size();
            }

            bool run(std::ostream &out = std::cout) {
                out << "\nBeginning: " << description << std::endl;
                for (auto c : cases) {
                    if (!c.passed()) {
                        c.output(out);
                        passed = false;
                    } else no_passed++;
                }
                report_details(total_cases(), no_passed, description, out, test_OK, test_FAIL);
                return passed;
            }

    };

    void run_test_suites(int trials);
}


#endif //CPP_POLY_TESTING_H
