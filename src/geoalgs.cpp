//
// Created by Ivan on 09-May-18.
//

#include <queue>
#include <set>
#include <omp.h>
#include "geoalgs.h"

// todo cleanup or fix shamos-hoey alg
struct Line {
    CGeometry::CPoint a,b;

    bool operator==(const Line &other) const {
        return this->operator<(other) && !other.operator<( *this );
    }

    bool operator<(const Line &other) const {
        CGeometry::CPoint   lp = CGeometry::POINT_MIN(a,b),
                            rp = CGeometry::POINT_MAX(a,b),
                            olp = CGeometry::POINT_MIN(other.a, other.b),
                            orp =CGeometry::POINT_MIN(other.a, other.b);

        if( lp.x <= olp.x ) {
            CGeometry::NumericType s = CGeometry::POINT_ON_LINE(lp, rp, olp);
            if( s != 0)
                return s > 0;
            else {
                if( lp.x == rp.x ) {
                    return lp.y < olp.y;
                }
                else  CGeometry::POINT_ON_LINE(lp, rp, orp) > 0;
            }
        } else {
            CGeometry::NumericType s = CGeometry::POINT_ON_LINE(olp, orp, lp);
            if( s != 0)
                return s < 0;
            else
                return CGeometry::POINT_ON_LINE(olp, orp, rp) < 0;
        }

    }

};

// todo cleanup or fix shamos-hoey alg
struct Event {
    Line l;
    bool left;
    int cid;

    bool operator<(const Event &other) const {
        return l < other.l;
    }
};


/**
 * Detects if any intersection between the line segments exists
 * @param omega - vector of connected lines
 * @return boolean - true if intersecting, false otherwise.
 */
bool Geomalgs::shamos_hoey(std::vector<CGeometry::CPoint> &omega) {
    CGeometry::NumericType nt = 0;
    return shamos_hoey(omega, nt);
}

/**
 * @param omega
 * @param edge_sum
 * @param area_sum
 * @return bool | true for crossing, false otherwise
 */
bool Geomalgs::bruteforce_validation(std::vector<CGeometry::CPoint> &omega, CGeometry::NumericType &edge_sum, CGeometry::NumericType &area_sum) {
    // todo benchmark the parallel section for fun (for small instances it should be fine)
    long long int idi = 0, idj = 0; //indexes into point vector
    int i,j;

    //omega is never changed just read only
    #pragma omp parallel shared(area_sum, edge_sum) private(i,j)
    for(i = 0; i < omega.size(); i++) {
        idi = (omega.size() + i) % omega.size();
        for(j = i+1; j < omega.size(); j++) { // edge compare to others
            idj = (omega.size() + j) % omega.size();
            // edge1(idi, idi+1) compared to edge(idj, idj+1)
            if(LINE_INTERSECT(omega[idi], omega[(idi + 1) % omega.size()],
                              omega[idj], omega[(idj + 1) % omega.size()])) return true;
        }
        //as we will traverse every edge this summation should cover both cases
        area_sum += LINE_AREA(omega[(omega.size() + i) % omega.size()], omega[(i + 1) % omega.size()]);
        edge_sum += LINE_LENGTH(omega[(omega.size() + i) % omega.size()], omega[(i + 1) % omega.size()]);
    }
    // LINE_AREA provides the rectangle
    area_sum = abs(area_sum) / 2;
    return false;
}


// todo FIX The algorithm - issues with the std::set and inserts
bool Geomalgs::shamos_hoey(std::vector<CGeometry::CPoint> &omega, CGeometry::NumericType &edge_sum) {
    std::priority_queue<Event> eq;
    std::set<Event> sl;
    std::_Rb_tree_const_iterator<Event> its[omega.size()];

    //sum circumference while also enqueueing the events in pq
    auto it = omega.begin();
    Event e1,e2;
    int cid = 0;
    Line line;
    line = Line{omega.back(), *it.operator->()};
    e1 = Event{line, true, cid};
    e2 = Event{line, false, cid++};
    eq.push( e1 );
    eq.push( e2 );
    edge_sum += LINE_LENGTH(line.a, line.b);
    it++;
    for(; it != omega.end()-1; it++) {
        line = Line{ *(it).operator->(), *(it+1).operator->() };
        e1 = Event{line, true, cid};
        e2 = Event{line, false, cid++};
        eq.push( e1 );
        eq.push( e2 );
        edge_sum += LINE_LENGTH(line.a, line.b);
    }
    std::cerr << "size of Q " << eq.size() << std::endl;

    Event e;
    std::pair<std::_Rb_tree_const_iterator<Event>, bool> s;
    std::_Rb_tree_const_iterator<Event> s_prev, s_next;
    while( !eq.empty() ) {
        e = eq.top();
        if (e.left) {
            s = sl.insert( e );
            its[ e.cid ] = s.first;

            s_prev = s.first--;
            s_next = s.first++;

            std::cerr <<  s_prev.operator->() << "  " << s_next.operator->() << "  " << std::endl;

            if( LINE_INTERSECT( e.l.a, e.l.b, s_next.operator->()->l.a, s_next.operator->()->l.b) )
                return false;
            if( LINE_INTERSECT( e.l.a, e.l.b, s_prev.operator->()->l.a, s_prev.operator->()->l.b) )
                return false;
        } else {
            s_prev = its[e.cid]--;
            s_next = its[e.cid]++;
            if( LINE_INTERSECT( s_next.operator->()->l.a, s_next.operator->()->l.b,
                                           s_prev.operator->()->l.a, s_prev.operator->()->l.b) )
                return true;

            sl.erase( its[e.cid] );
           // its[e.cid] = nullptr;
        }

        eq.pop();
    }
    return true;
}

/**
 * For area: multiple sources refer to greens theorem for finding area of arbitrary
 * closed non-intersecting polygons
 * (shoelace - speciliased case of greens theorem)
**/
CGeometry::NumericType Geomalgs::shoelace(std::vector<CGeometry::CPoint> &lines){
    return 0;
}