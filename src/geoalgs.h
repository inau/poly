//
// Created by Ivan on 09-May-18.
//

#ifndef CPP_POLY_GEOALGS_H
#define CPP_POLY_GEOALGS_H



#include <vector>
#include "polygon.h"

namespace Geomalgs {

    /**************************
        Utility functions
    *************************/
    static CGeometry::NumericType LINE_LENGTH(const CGeometry::CPoint &a, const CGeometry::CPoint &b) {
        if( a == b ) return 0;

        CGeometry::NumericType dx = (POINT_MAX(a, b).x - POINT_MIN(a, b).x ),
                dy = (POINT_MAX(a, b).y - POINT_MIN(a, b).y );

        return  dx != 0 && dy != 0 ? (CGeometry::NumericType)hypot(dx, dy) :
                dx == 0 ? dy : dx;
    }

    /**
     * area of a line (consider the rectangle defined by the two coordinates)
     * @param a
     * @param b
     * @return the area of an rectangle defined by the points
     */
    static CGeometry::NumericType LINE_AREA(const CGeometry::CPoint &a, const CGeometry::CPoint &b) {
        return (CGeometry::NumericType)(a.x+b.x)*(a.y-b.y);
    }

    /**
     * check if two lines ab and a2b2 intersect
     * @param a
     * @param b
     * @param a2
     * @param b2
     * @return true on intersect
     */
    static bool LINE_INTERSECT(const CGeometry::CPoint &a, const CGeometry::CPoint &b,
                               const CGeometry::CPoint &a2, const CGeometry::CPoint &b2 ) {
        //check if edges are 'connected' in one end
        if(b == a2 || a == b2 || a == a2 || b == b2)
            return false;

        //check for intersect by using line position in regards to endpoints
        CGeometry::NumericType l, r, l2, r2;
        l = POINT_ON_LINE( POINT_MIN(a,b), POINT_MAX(a,b), POINT_MIN(a2,b2));
        r = POINT_ON_LINE( POINT_MIN(a,b), POINT_MAX(a,b), POINT_MAX(a2,b2));
        // the results of point on line give an indication of where the line end points are compared to ab
        // if both are positive or negative the multiplication gives a positive result and indicates that they are on same side
        if(l*r > 0)
            return false;

        // repeat for a2b2
        l2 = POINT_ON_LINE( POINT_MIN(a2,b2), POINT_MAX(a2,b2), POINT_MIN(a,b));
        r2 = POINT_ON_LINE( POINT_MIN(a2,b2), POINT_MAX(a2,b2), POINT_MAX(a,b));
        if(l2*r2 > 0)
            return false;

        //same line (all points on same line)
        if( (l2 + r2 + l + r) == 0 )
            return false;

        return true;
    }

    /**************************
      Intersection Algorithms
    *************************/
    bool bruteforce_validation(std::vector<CGeometry::CPoint> &omega, CGeometry::NumericType &edge_sum, CGeometry::NumericType &area_sum);

    bool shamos_hoey(std::vector<CGeometry::CPoint> &omega);
    bool shamos_hoey(std::vector<CGeometry::CPoint> &omega, CGeometry::NumericType &edge_sum);

    /**************************
      Area Algorithms
    *************************/
    CGeometry::NumericType shoelace(std::vector<CGeometry::CPoint> &lines);

}
#endif //CPP_POLY_GEOALGS_H
