
/*Implement the following and add appropriate test cases to main():
  (1) Add a class, CSquare, that represents a square
  (2) To each class add a method that calculates the circumference
  (3) Add a compare operator, >, that compares the area of two objects:
      E.g. the expression (rect > trgl) returns true if the area of rect is larger than trgl.
  (4) Generalize the CPolygon class to represent an arbitrary polygon with N line segments.
      Modify the area and circumference methods accordingly. Implement 
	  constructors and destructors.
  (5) Add a check for crossing line segments and a suitable way to abort the construction
      in this situation.
*/

#include <iostream> 
using namespace std;

class CPolygon {
  protected:
    int width, height;
  public:
    void set_values (int a, int b)
      { width=a; height=b;}
  };

class COutput {
  public:
    void output (int i);
  };

void COutput::output (int i) {
  cout << i << endl;
  }

class CRectangle: public CPolygon, public COutput {
  public:
    int area (void)
      { return (width * height); }
  };

class CTriangle: public CPolygon, public COutput {
  public:
    int area (void)
      { return (width * height / 2); }
  };
  
int main () {
  CRectangle rect;
  CTriangle trgl;
  rect.set_values (4,5);
  trgl.set_values (4,5);
  rect.output (rect.area());
  trgl.output (trgl.area());
  return 0;
}

